@extends('layouts.base')

@section('content')
	<h1>My Post</h1>
	
	<a href="{{ route('create-post') }}" class="btn btn-success">Create Post</a>

	<ul>
		@foreach($list_posts as $post)
			<li>
				<a href="{{route('detail-post', $post->slug)}}">{{ $post->title }}</a>
			</li>
		@endforeach
	</ul>

	@yield('content-detail')
@stop