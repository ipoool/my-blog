@extends('layouts.base')

@section('content')

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<h2>Create Post</h2>
	<form action="{{ route('save-post') }}" method="post">
		{{ csrf_field() }}
		<input type="text" class="form-control" name="title">
		<br>
		<textarea class="form-control" name="content"></textarea>
		<br>
		<button type="submit" class="btn btn-primary">Save</button>
	</form>
@stop
